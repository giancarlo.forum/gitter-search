const Msg = res =>
  m.mount(document.getElementById('result'), {
    view: v =>
      res.map(chat =>
      m("article.message.is-info",
        m(".message-header",
          m("a", {
            href: `https://gitter.im/${store.room}?at=${chat.messageId}`,
            target: '_blank'
          }, chat.messageId),
        chat.time),
        m(".message-body",
          m("strong", chat.username),
          m('p', chat.text),
        )
      )
    )
})

export default Msg
