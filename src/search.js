import Msg from './msg'

function getDay$(dt){
  const appendDt = dateFns.format(dt, 'YYYY/MM/DD')

  return Rx.Observable.ajax({
    url: `https://cors-new.now.sh/https://gitter.im/${store.room || 'mithriljs/mithril.js'}/archives/${appendDt}`,
    responseType: 'text'
  })
}

function Search(){
  const startDt = new Date(store.init)
  const endDt   = new Date(store.end)
  const dates   = dateFns.eachDay(startDt, endDt)

  Rx.Observable.from(dates)
  .flatMap(dt => getDay$(dt).pluck('response'))
  .flatMap(response => {
    const domParser = new DOMParser()
    const doc = domParser.parseFromString(response, "text/html")
    return Rx.Observable.from(doc.querySelectorAll('.chat-app #chat-container .chat-item'))
  })
  .map(node => {
    const user = node.querySelector(".chat-item__from")
    const time = node.querySelector(".chat-item__time.js-chat-time")
    const linkIdClass =
      Object.values(node.classList)
      .filter(it => new RegExp("^model-id-").test(it))
    return {
      node,
      messageId: linkIdClass.length == 1 ? linkIdClass[0].split('-')[2] : undefined,
      name: user ? node.querySelector(".chat-item__from.js-chat-item-from").innerHTML : 'N/A',
      username: user ? node.querySelector(".chat-item__username.js-chat-item-from").innerHTML : 'N/A',
      text: node.querySelector(".chat-item__text.js-chat-item-text").innerText,
      time: time ? time.innerText : 'N/A'
      
    }
  })
  .filter(chat => store.user ? new RegExp(store.user).test(chat.username)  : true) // or return true for all users
  .filter(chat => store.text ? new RegExp(store.text, 'i').test(chat.text) : true) // or return true for all
  .toArray()
  .subscribe({
    next:  res => Msg(res),
    error: err => console.log(err)
  })
}

export default Search
