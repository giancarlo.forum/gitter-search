import Search from './search'

const rows = [
  [ {store: 'room', label: 'Room name'},
    {store: 'user', label: 'User nick', ph: '@osban', icon: true} ],
  [ {store: 'init', label: 'Initial date', type: 'date'},
    {store: 'end',  label: 'End date', type: 'date'} ],
  [ {store: 'text', label: 'Text'} ]
]

const Form =
  m(".columns", {style: {margin:"10px 25px"}},
    m(".column.is-8",
      m("article.message.is-info",
        m(".message-header",
          m("h2", "Gitter Search")
        ),
        m(".message-body.field",
          rows.map(row =>
            m(".columns",
              row.map(y =>
                m(".column",
                  m("label.label", y.label),
                  m(`.control${y.icon && '.has-icons-left'}`,
                    m("input.input", {
                      type: y.type || 'text',
                      placeholder: y.ph || '',
                      onchange(e){store[y.store] = e.target.value},
                      value: store[y.store]
                    }),
                    y.icon &&
                    m("span.icon.is-small.is-left",
                      m("i.fa.fa-user")
                    )
                  )
                )
              )
            )
          ),
          m("a.button.is-info#search", {onclick: Search},
            m("span.icon",
              m("i.fa fa-search")
            ),
            m("span", "Search")
          )
        )
      )
    )
  )

m.mount(document.getElementById('form'), {view: v => Form})
